ThreadMailbox
-------------

Threadmailbox and threadmanager are helper classes which wrap thread registration
and message sending into nice and convinient API calls. The unrerlying implementation
relies on pthread API, and the implementation works on both POSIX based systems
where pthread library is available as well as on Windows platform using phthread-win32/64.

Semaphores and phtread signals are used by the implementation. The mailbox implementation
implements both blocking and non-blocking message queues.

This is a single headerfile implementation, which makes it easy to take into use.

Example application is provided in main.cpp and a very nifty makefile for compilation
on Unix based systems.
