
#ifndef THREADMAILBOX_H
#define THREADMAILBOX_H

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <map>
#include <vector>


/// MESSAGESTATUS is a status enumeration for internal messageHeap message
/// status.
typedef enum {
    MESSAGESTATUS_FREE = 0,
    MESSAGESTATUS_RESERVED
} MESSAGESTATUS;

/// MESSAGEID is a harmonized enumeration serving the harmonized messageHeap.
/// Following message IDs are defined for reference.
typedef enum {
    MESSAGEID_TERMINATE = 99,
    MESSAGEID_M1,
    MESSAGEID_M2,
    MESSAGEID_M3
} MESSAGEID;

/// MESSAGE is a harmonized message structure which is used in message
/// dispatching. Having a single message structure allows pre-definition
/// of the messageHeap size, and hence avoid any dynamic memory allocation.
typedef struct __message {
    MESSAGESTATUS status;
    MESSAGEID mID;
    union {
        struct {
            unsigned int data;
        } m1;
        struct {
            int data1;
            int data2;
        } m2;
    } payload;
} MESSAGE;


/*
 * ncMailbox is a signalling implementation utilized by the threads and the 
 * threadmanager. It acts as a storage container for any message that is 
 * sent between the threads. The mailbox implements blocking and nonblocking
 * message reception, and blocked (semaphore sleeping) threads are woken up
 * using pthread signals.
 */

class ncMailbox
{
public:
    /// ncMailBox construction and destruction
    ncMailbox() {
        pthread_mutex_init(&mboxMutex, NULL);
        pthread_cond_init(&mboxCondition, NULL);
    }
    ~ncMailbox() {
        pthread_mutex_destroy(&mboxMutex);
        pthread_cond_destroy(&mboxCondition);
    }
    /// ::sendMessage(): allows external parties to send a message to
    /// this mailbox
    int sendMessage(MESSAGE *msg) {
        pthread_mutex_lock(&mboxMutex);
        mailbox.push_back(msg);
        pthread_cond_signal(&mboxCondition);
        pthread_mutex_unlock(&mboxMutex);
        return 0;
    }
    /// ::receiveMessage(): allows the owner to read (and optionally wait)
    /// for new messages
    MESSAGE *receiveMessage(bool block) {
        pthread_mutex_lock(&mboxMutex);
        if (mailbox.size() == 0) {
            if (block == true) {
                pthread_cond_wait(&mboxCondition, &mboxMutex);
            }
            else {
                pthread_mutex_unlock(&mboxMutex);
                return NULL;
            }
        }
        MESSAGE *msg = mailbox[0];
        mailbox.erase(mailbox.begin());
        pthread_mutex_unlock(&mboxMutex);
        fprintf(stdout, " pulling a message from mailbox, %d messages remaining\n", (int)mailbox.size());
        return msg;
    }
private:
    std::vector <MESSAGE *> mailbox;    // Mailbox itself
    pthread_mutex_t mboxMutex;          // Mutex to protect mailbox access
    pthread_cond_t mboxCondition;       // Condition variable for the mailbox.
};


/*
 * ncThreadManager is a helper class (singleton) of which purpose is the help
 * creating threads and enable them to use a couple of simple services. The
 * services include message heap and message allocation, message sending,
 * thread registration and unregistration.
 */

class ncThreadManager
{
public:
    /// ncThreadManager() destruction
    ~ncThreadManager() {
        pthread_mutex_destroy(&threadMutex);
        pthread_mutex_destroy(&messageMutex);
    }
    static ncThreadManager * Instance();
    /// ::registerThread(): allows any external thread to register its mailbox
    /// in the threadmap, and hence allow other threads to dispatch messages to
    /// this mailbox.
    void registerThread(unsigned int tID, ncMailbox *box) {
        pthread_mutex_lock(&threadMutex);
        std::map <unsigned int, ncMailbox*>::iterator it = threads.find(tID);
        if (it != threads.end()) {
            fprintf(stderr, "Thread ID=%d already in the threadmap\n", tID);
            return;
        }
        fprintf(stdout, "Registering new thread id=%d mailbox ptr=%p\n", tID, box);
        threads.insert(std::pair<unsigned int, ncMailbox *>(tID, box));
        pthread_mutex_unlock(&threadMutex);
    }
    /// ::unregisterThread(): allows external threads to unregister themselves
    /// from the pool. After this call no more messages are delivered to the
    /// mailbox.
    void unregisterThread(unsigned int tID) {
        pthread_mutex_lock(&threadMutex);
        std::map <unsigned int,ncMailbox*>::iterator it = threads.find(tID);
        if (it == threads.end()) return;
        threads.erase(it);
        fprintf(stdout, "Thread ID=%d unregistering, now %d threads remaining\n", tID, (int)threads.size());
        pthread_mutex_unlock(&threadMutex);
    }
    /// ::sendMessage(): allows external parties to dispatch messages based
    /// on destination threadID. Message structure is allocated prior sending with
    /// allocMessage().
    int sendMessage(unsigned int tID, MESSAGE *msg) {
        pthread_mutex_lock(&threadMutex);
        std::map <unsigned int, ncMailbox*>::iterator it = threads.find(tID);
        if (it == threads.end()) {
            pthread_mutex_unlock(&threadMutex);
            fprintf(stderr, "No mailbox for thread id=%d\n", tID);
            return -1;
        }
        int rc = (*it).second->sendMessage(msg);
        pthread_mutex_unlock(&threadMutex);
        return rc;
    }
    /// ::terminateThreads(): this method will send a special termination message
    /// to all threads currently in the threadmap. The message is a 
    void terminateThreads(void) {
        std::map<unsigned int, ncMailbox *>::iterator it;
        //pthread_mutex_lock(&threadMutex);
        for (it=threads.begin(); it!=threads.end(); ++it) {
            MESSAGE *msg = allocMessage();
            msg->mID = MESSAGEID_TERMINATE;
            sendMessage((*it).first, msg);
        }
        //pthread_mutex_unlock(&threadMutex);
    }
    /// ::allocMessage(): all messages are allocated as a heap in ncThreadManager
    /// private areas. This method avoids all dynamic memory allocations in
    /// message dispatching in the expense of linear lookups of free message
    /// slots. However, in regular case, this is not seen as a problem since
    /// in general, it is expected only a few messages to wait in the queue
    /// at any given time, and hence the traversal is not time consuming.
    MESSAGE * allocMessage(void) {
        pthread_mutex_lock(&messageMutex);
        for (unsigned int i=0; i<MSGHEAP_SIZE; i++) {
            if (messageHeap[i].status == MESSAGESTATUS_FREE) {
                messageHeap[i].status = MESSAGESTATUS_RESERVED;
                pthread_mutex_unlock(&messageMutex);
                fprintf(stdout, "reserving msg index %d\n", i);
                return &messageHeap[i];
            }
        }
        pthread_mutex_unlock(&messageMutex);
        return NULL; // This is fatal, but very very unlikely to happen
    }
    /// ::freeMessage(): when the allocated message is consumed, its memory
    /// needs to be freed for other threads.
    void freeMessage(MESSAGE *msg) {
        pthread_mutex_lock(&messageMutex);
        msg->status = MESSAGESTATUS_FREE;
        pthread_mutex_unlock(&messageMutex);
    }
private:
    /// ::ncThreadManager() singleton private construction
    ncThreadManager() {
        memset(messageHeap, 0, sizeof(messageHeap));
        for (unsigned int i=0; i<MSGHEAP_SIZE; i++) {
            messageHeap[i].status = MESSAGESTATUS_FREE;
        }
        pthread_mutex_init(&threadMutex, NULL);
        pthread_mutex_init(&messageMutex, NULL);
    }
    ncThreadManager(ncThreadManager const &) { }
    static ncThreadManager *p_Instance;             // singleton instance placeholder
    static const unsigned int MSGHEAP_SIZE = 128;   // maximum number of messages in the heap
    std::map <unsigned int, ncMailbox *> threads;   // threadMap
    pthread_mutex_t threadMutex;                    // threadMap mutex
    MESSAGE messageHeap[MSGHEAP_SIZE];              // messageHeap
    pthread_mutex_t messageMutex;                   // messageHeap mutex
};
ncThreadManager * ncThreadManager::p_Instance = NULL;
ncThreadManager * ncThreadManager::Instance() {
    if (p_Instance == NULL) {
        p_Instance = new ncThreadManager();
    }
    return p_Instance;
}

#endif // THREADMAILBOX_H
