
#include "ThreadMailbox.h"

/*
 * This is a demo application, which uses implemented threadmanager and threadmailbox
 * for signalling between three demo threads. The main app starts three threads, and
 * each thread allocates a mailbox. Then the main application sends a message to one
 * of the threads invoking a message ping-pong effect. Threads use blocking message
 * reception service of the threadmanager and threadmailbox, so a lot of functionality
 * like semaphores and signals are hidden from the threads into the threadmanager.
 *
 * Upon start the main app sleeps for one second, then stars the messahe ping-pong,
 * and after another second stops the threads.
 */

// "Unique" thread IDs for the sake of testing. We define three threads here
// which play the message ping-pong for demonstration purposes:

#define THREADID_T1     (1)
#define THREADID_T2     (2)
#define THREADID_T3     (3)

// Thread 1:
void *thread_RM(void *threadid)
{
    // Unique ID
    unsigned int tID = THREADID_T1;

    // Allocate new threadmailbox for this thread:
    ncMailbox * box = new ncMailbox();

    MESSAGE *msg;
    bool terminate = false;
    fprintf(stdout, "Thread starting. id=%d, mailbox ptr=%p\n", tID, box);

    // Register this thread to the threadmanager (together with the mailbox)
    ncThreadManager::Instance()->registerThread(tID, box);

    // Thread mainloop:
    while (1) {

        // Start waiting messages block thread
        msg = box->receiveMessage(true);

        while (msg != NULL) { // consume all messages in the box
            
            fprintf(stdout, "Thread id=%d got a message id=%d\n", tID, msg->mID);
            if (msg->mID == MESSAGEID_TERMINATE) terminate = true;
            if (msg->mID == MESSAGEID_M1) {

                // In case we received message ID == M1, then send similar message
                // to thread #2.
                MESSAGE *m = ncThreadManager::Instance()->allocMessage();
                m->mID = MESSAGEID_M1;
                if (0 != ncThreadManager::Instance()->sendMessage(THREADID_T2, m)) {
                    // In case of failure, let's free the allocated message:
                    ncThreadManager::Instance()->freeMessage(msg);
                }
            }

            // Once consumed, the received message can be freed:
            ncThreadManager::Instance()->freeMessage(msg);

            // Get next message from the box, if there are any, and consume
            // them all in this loop
            msg = box->receiveMessage(false);
        }
        if (terminate == true) break;
    }

    // On exit, unregister and delete mailbox.
    fprintf(stdout, "Thread %d exiting\n", tID);
    ncThreadManager::Instance()->unregisterThread(tID);
    delete box;
    pthread_exit(NULL);
}

// Thread 2:
void *thread_RC(void *threadid)
{
    unsigned int tID = THREADID_T2;
    ncMailbox * box = new ncMailbox();
    MESSAGE *msg;
    bool terminate = false;
    fprintf(stdout, "Thread starting id=%d, mailbox ptr=%p\n", tID, box);
    ncThreadManager::Instance()->registerThread(tID, box);
    while (1) {
        msg = box->receiveMessage(true);
        while (msg != NULL) { // consume all messages in the box
            fprintf(stdout, "Thread id=%d got a message id=%d\n", tID, msg->mID);
            if (msg->mID == MESSAGEID_TERMINATE) terminate = true;
            if (msg->mID == MESSAGEID_M1) {
                MESSAGE *m = ncThreadManager::Instance()->allocMessage();
                m->mID = MESSAGEID_M1;
                if (0 != ncThreadManager::Instance()->sendMessage(THREADID_T3, m)) {
                    ncThreadManager::Instance()->freeMessage(msg);
                }               
            }
            ncThreadManager::Instance()->freeMessage(msg);
            msg = box->receiveMessage(false);
        }
        if (terminate == true) break;
    }
    fprintf(stdout, "Thread %d exiting\n", tID);
    ncThreadManager::Instance()->unregisterThread(tID);
    delete box;
    pthread_exit(NULL);
}

// Thread 3:
void *thread_IN(void *threadid)
{
    unsigned int tID = THREADID_T3;
    ncMailbox * box = new ncMailbox();
    MESSAGE *msg;
    bool terminate = false;
    fprintf(stdout, "Thread starting id=%d, mailbox ptr=%p\n", tID, box);
    ncThreadManager::Instance()->registerThread(tID, box);
    while (1) {
        msg = box->receiveMessage(true);
        while (msg != NULL) { // consume all messages in the box
            fprintf(stdout, "Thread %d got a message id=%d\n", tID, msg->mID);
            if (msg->mID == MESSAGEID_TERMINATE) terminate = true;
            if (msg->mID == MESSAGEID_M1) {
                MESSAGE *m = ncThreadManager::Instance()->allocMessage();
                m->mID = MESSAGEID_M1;
                if (0 != ncThreadManager::Instance()->sendMessage(THREADID_T1, m)) {
                    ncThreadManager::Instance()->freeMessage(msg);
                }               
            }
            ncThreadManager::Instance()->freeMessage(msg);
            msg = box->receiveMessage(false);
        }
        if (terminate == true) break;
    }
    fprintf(stdout, "Thread %d exiting\n", tID);
    ncThreadManager::Instance()->unregisterThread(tID);
    delete box;
    pthread_exit(NULL);
}

int main (int argc, char *argv[])
{
    pthread_t th_RM, th_RC, th_IN;
    unsigned int rc;

    rc = pthread_create(&th_RM, NULL, thread_RM, NULL);
    rc = pthread_create(&th_RC, NULL, thread_RC, NULL);
    rc = pthread_create(&th_IN, NULL, thread_IN, NULL);

    // Sleep for a second:
    usleep(1 * 1000 * 1000);

    // Allocate new message from the manager:
    MESSAGE *m = ncThreadManager::Instance()->allocMessage();
    // Setup message ID
    m->mID = MESSAGEID_M1;
    // Send asynchronous message the Thread 1. This starts a ping-pong message test
    // between threads' mailboxes to simulate action.
    ncThreadManager::Instance()->sendMessage(THREADID_T1, m);

    // Sleep another second:
    usleep(1 * 1000 * 1000);

    // Invoke thread termination:
    fprintf(stdout, "Now starting destruction\n");
    ncThreadManager::Instance()->terminateThreads();

    // Final exit:
    pthread_exit(NULL);
}
